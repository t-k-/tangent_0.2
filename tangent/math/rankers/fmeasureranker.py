"""
    Tangent
    Copyright (c) 2013 David Stalnaker, Richard Zanibbi

    This file is part of Tangent.

    Tanget is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tangent is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tangent.  If not, see <http://www.gnu.org/licenses/>.

    Contact:
        - David Stalnaker: david.stalnaker@gmail.com
        - Richard Zanibbi: rlaz@cs.rit.edu
"""

from __future__ import division
from collections import Counter


class FMeasureRanker(object):
    """
    FMeasureRanker calculate the fscore between two expressions's symbol tree

    Precision = percentage of matched symbols
    Recall = percentage of symbol pairs in the query that are in match

    FMeasure = 2|M| /  ( |Q| + |R|)

    Q= set of pairs in the query
    R= set of pairs in the result candidate
   |M| = set of pairs that matched
    """

    @staticmethod
    def search_score(search_pairs, pair_counts=None, total_exprs=None):
        """
        Maximum score for index pairs is the number of symbol pairs

        :type search_pairs: list
        :param search_pairs: list of symbol pairs

        :rtype: int
        :return: number of symbol pairs

        """
        return len(search_pairs)

    result_score_key = 'fmeasure_score'
    fetch_paths = False

    @staticmethod
    def rank(match_pairs, search_score, result_score, pair_counts, total_exprs, search_paths):
        """
        Returns f-score

        :type match_pairs: list
        :param match_pairs list of pairs that latched

        :type search_score: double
        :param search_score: score for pairs in query

        :type result_score: double
        :param result_score: score for pairs that matched


        :type pair_counts: dict(str,int)
        :param pair_counts: frequency for each symbol pair

        :type total_exprs:
        :param total_exprs:

        :type search_paths:dict(str,list)
        :param: search_paths:given two symbol pairs, the path between them

        :rtype: double
        :return: fscore

        """
        num_matches = len(match_pairs)
        return 2 * num_matches / (search_score + result_score)


    @staticmethod
    def rank2(exp1_pairs, query_pairs):
        matched_pairs = []
        unmatched_pairs_in_q = []
        wildcard_pairs = {}
        wildcard_pairs_left = {}
        wildcard_pairs_right = {}

        wildcard_pairs_left_other_symb = set()
        wildcard_pairs_right_othersymb = set()
        not_at_all_pairs = []

        exp1_pairs_unseen = Counter(exp1_pairs)
        query_pairs_counter = Counter(query_pairs)

        for key in query_pairs_counter.keys():  # for all query keys
            if key in exp1_pairs_unseen:
                exp_count = exp1_pairs_unseen[key]
                q_count = query_pairs_counter[key]
                matched_count = exp_count
                was_greater = False
                if exp_count > q_count:
                    matched_count = q_count
                    was_greater = True

                for i in range(0, matched_count):
                    matched_pairs.append(key)

                if was_greater:
                    for i in range(0, exp_count - matched_count):
                        not_at_all_pairs.append(key)

                exp1_pairs_unseen.pop(key, None)
            else:
                # if key is qvar
                pair_left = key.split('|')[0]
                pair_right = key.split('|')[1]

                h_distance = key.split('|')[2]
                v_distance = key.split('|')[3]

                p = None
                was_right=False

                if "qvar" in pair_left:
                    p = pair_left
                    was_right=False


                if "qvar" in pair_right:
                    p=pair_right
                    was_right=True

                if p is not None:


                    if was_right:
                        wildcard_pairs_right[p] = wildcard_pairs_right.get(p, [])
                        for i in range(0, query_pairs_counter[key]):
                            wildcard_pairs_right[p].append(key)
                            wildcard_pairs_right_othersymb.add((pair_left,h_distance,v_distance))
                    else:
                        wildcard_pairs_left[p] = wildcard_pairs_left.get(p, [])
                        for i in range(0, query_pairs_counter[key]):
                            wildcard_pairs_left[p].append(key)
                            wildcard_pairs_left_other_symb.add((pair_right,h_distance,v_distance))



                else:
                    for i in range(0, query_pairs_counter[key]):
                        unmatched_pairs_in_q.append(key)




        # for all wildcard pairs

        unique_wildcards = set(wildcard_pairs_left.keys()).union(set(wildcard_pairs_right.keys()))

        for time in range(0, len(unique_wildcards)):  # for each wildcard
            wildcard_match_count = []
            wildcard_match_count_no_direction = []

            unique_wildcards = set(wildcard_pairs_left.keys()).union(set(wildcard_pairs_right.keys()))
            for wildcard in unique_wildcards:
                # try_matching_left = True
                #try_matching_both=False
                #if (len(wildcard_pairs_right.get(wildcard, [])) > len(wildcard_pairs_left.get(wildcard, []))):
                #    try_matching_left = False
                #elif (len(wildcard_pairs_right.get(wildcard, [])) == len(wildcard_pairs_left.get(wildcard, []))):
                #    try_matching_both=True



                for key in exp1_pairs_unseen.elements():
                    pair_left = key.split('|')[0]
                    pair_right = key.split('|')[1]
                    h_distance = key.split('|')[2]
                    v_distance = key.split('|')[3]

                    #   if try_matching_both:
                    #      wildcard_match_count.append((wildcard, pair_left, 'left'))
                    #       wildcard_match_count.append((wildcard, pair_right, 'right'))
                    #   elif try_matching_left:



                    if pair_left==pair_right: #if both wildcards,check right
                        if (wildcard,h_distance,v_distance) in wildcard_pairs_right_othersymb :
                            wildcard_match_count.append((wildcard, pair_right, 'right'))
                            wildcard_match_count_no_direction.append((wildcard, pair_right))
                    else:
                        if (pair_right,h_distance,v_distance) in wildcard_pairs_left_other_symb:
                            wildcard_match_count.append((wildcard, pair_left, 'left'))
                            wildcard_match_count_no_direction.append((wildcard, pair_left))
                        #  else:
                        if (pair_left,h_distance,v_distance) in wildcard_pairs_right_othersymb :
                            wildcard_match_count.append((wildcard, pair_right, 'right'))
                            wildcard_match_count_no_direction.append((wildcard, pair_right))


            if len(wildcard_match_count)!=0:
                wildcard_counter = Counter(wildcard_match_count)
                elem = wildcard_counter.most_common(1)[0]
                wildcard_counter_no_direction = Counter(wildcard_match_count_no_direction)



                mc=wildcard_counter_no_direction.most_common(2)
                if len(mc)==2:

                    elem1= wildcard_counter_no_direction.most_common(2)[0]
                    elem2 = wildcard_counter_no_direction.most_common(2)[1]

                    if (elem1[1]==elem2[1]):#same count
                        wq1,_=elem1[0]
                        wq2,_=elem2[0]

                        if wq1==wq2:#same element....means different equal binding
                             return 0,"","",""


                times_wildcard_possibility=len(wildcard_pairs_left.get(wildcard,[]))+len(wildcard_pairs_right.get(wildcard,[]))
                times_appeared=wildcard_counter_no_direction.most_common(1)[0][1]#count of how often the element occured

                if (times_appeared<times_wildcard_possibility):
                    return 0, 0, 0, 0



                # now found most common element, lets unify
                (wildcard, symbol, pos), count = elem
                new_pairs_to_match = {}




                if wildcard in wildcard_pairs_left:
                    for key in wildcard_pairs_left[wildcard]:
                        pair_right = key.split('|')[1]
                        hpos = key.split('|')[2]
                        vpos = key.split('|')[3]

                        #if pos != 'left':
                        #    unmatched_pairs_in_q.append(key)
                        #else:
                        new_key = symbol + "|" + pair_right + "|" + hpos + "|" + vpos

                        new_pairs_to_match[new_key] = new_pairs_to_match.get(new_key, 0) + 1

                    wildcard_pairs_left.pop(key, None)

                if wildcard in wildcard_pairs_right:
                    for key in wildcard_pairs_right[wildcard]:
                        pair_left = key.split('|')[0]
                        hpos = key.split('|')[2]
                        vpos = key.split('|')[3]

                        #if pos != 'right':
                        #    unmatched_pairs_in_q.append(key)
                        #else:

                        if pair_left !=wildcard:
                            new_key = pair_left + "|" + symbol + "|" + hpos + "|" + vpos

                            new_pairs_to_match[new_key] = new_pairs_to_match.get(new_key, 0) + 1
                        else:#symbol on left and right
                            new_key = symbol + "|" + symbol + "|" + hpos + "|" + vpos
                            new_pairs_to_match[new_key] = new_pairs_to_match.get(new_key, 0) + 1


                    wildcard_pairs_right.pop(wildcard, None)




                all_keys = list(new_pairs_to_match.keys())
                for key in all_keys:
                    if key in exp1_pairs_unseen:
                        exp_count = exp1_pairs_unseen[key]
                        q_count = new_pairs_to_match[key]
                        matched_count = exp_count
                        was_greater = False
                        if exp_count > q_count:
                            matched_count = q_count
                            was_greater = True

                        for i in range(0, matched_count):
                            matched_pairs.append(key)
                            new_pairs_to_match[key] = new_pairs_to_match[key] - 1

                        if was_greater:
                            for i in range(0, matched_count - matched_count):
                                not_at_all_pairs.append(key)
                                unmatched_pairs_in_q.append(key)

                        exp1_pairs_unseen.pop(key, None)

        score = 2 * len(matched_pairs) / (len(query_pairs) + len(exp1_pairs))

        return score, 0, 0, 0