"""
    Tangent
    Copyright (c) 2013 David Stalnaker, Richard Zanibbi

    This file is part of Tangent.

    Tanget is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tangent is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tangent.  If not, see <http://www.gnu.org/licenses/>.

    Contact:
        - David Stalnaker: david.stalnaker@gmail.com
        - Richard Zanibbi: rlaz@cs.rit.edu
"""
import io
import pickle
import xml
import os
import xml.etree.ElementTree as ET
from collections import Counter
from sys import argv

import requests as req
import sys

from tangent.common.utility.math_extractor import MathExtractor
from tangent.math.models.exceptions import UnknownTagException
from tangent.math.models.mathsymbol import MathSymbol
from tangent.math.mathml.latex_mml import ServerLatexToMathML, LatexToMathML
from tangent.common.search.model.Stats import Stats


ET.register_namespace('', 'http://www.w3.org/1998/Math/MathML')


class SymbolTree:
    """
    Symbol Tree manipulation and parsing

    Uses latexmlmath (http://dlmf.nist.gov/LaTeXML/index.html) to create the presentation mml

    """
    __slots__ = ['root', 'latex', 'mathml', 'document', 'position']

    def __init__(self, root):
        self.root = root
        self.root.generate_ids()

    def get_pairs(self, get_paths=False):
        """
        Return list of symbols

        :rtype: list
        :return list of symbols
        """

        assert (self.root, MathSymbol)
        # if self.root.matrix == True:
        # return self.getMatrixPairs(get_paths)


        if get_paths:
            pairs = []
            paths = []
            for s1, s2, dh, dv, path in self.root.get_pairs():
                p = '|'.join([s1.replace('|', '!@!'), s2.replace('|', '!@!'), str(dh), str(dv)])

                # p = p.encode('unicode-escape').decode('utf-8')

                if not (len(p)>200):
                    pairs.append(p)
                    paths.append(''.join(str(path)))
                else:
                    print("pair "+p +"is longer than 200 characters" , file=sys.stderr)
            return pairs, paths
        else:
            pairs = []
            for s1, s2, dh, dv, _ in self.root.get_pairs():
                p='|'.join([s1.replace('|', '!@!'), s2.replace('|', '!@!'), str(dh), str(dv)])
                if not (len(p)>200):
                    pairs.append(p)
                else:
                    print("pair "+p +"is longer than 200 characters" , file=sys.stderr)


            return pairs


    def get_symbols(self):
        return self.root.get_symbols()

    @classmethod
    def parse(cls, filename, file_id, missing_tags=None, problem_files=None):
        """
        Extract symbols tree from file

        :type filename: string
        :param filename: directory to seach in

        :rtype: list(SymbolTree)
        :return list of Symbol trees
        """
        ext = os.path.splitext(filename)[1]
        if ext == '.tex':
            with open(filename) as f:
                return [cls.parse_from_tex(f.read(), file_id)]
        elif ext in {'.xhtml', '.mathml', '.mml', '.html'}:
            return cls.parse_all_from_xml(filename, file_id, missing_tags=missing_tags, problem_files=problem_files)
        else:
            print(('Unknown filetype for %s' % filename))
            return []

    @classmethod
    def parse_from_tex(cls, tex, file_id=-1):
        """
        Parse expression from tex string using latexmlmath to convert ot presentation markup language


        :param tex tex string
        :type tex string

        :rtype SymbolTree
        :return SymbolTree

        """

        output=LatexToMathML.convert_to_mathml(tex)
        #output = ServerLatexToMathML.convert_to_mathml(tex)

        # url = 'http://halifax.cs.rit.edu:8324/'
        # tex = tex.decode('UTF-8')
        # image to query
        # pickled data
        # data = pickle.dumps(tex)

        #make equest
        #r = req.get(url, data=data)
        #load the numpy array
        #output = pickle.loads(r.text)
        #output = output.decode('UTF-8')

        #p = subprocess.Popen('latexmlmath -pmml - -', shell=True, stdout=subprocess.PIPE, stdin=subprocess.PIPE,
        #                     stderr=open(os.devnull, 'r'))
        #(output, _) = p.communicate(input=tex)
        #f = StringIO.StringIO(output)
        return cls.parse_from_mathml(output)


    @classmethod
    def convert_to_presentation_mml(cls, tex):
        url = 'http://halifax.cs.rit.edu:8324/'
        data = pickle.dumps(tex)
        r = req.get(url, data=data)
        # load the numpy array
        try:
            output = pickle.loads(r.text)
            output = output.decode('utf-8')
            return output
        except Exception as e:
            z = 9



            # @classmethod
            # def parse_from_mathml_string(cls, mathml):
            # """
            # Parse expression from mathml string


            # :param elem mathml string

            #   :rtype SymbolTree
            #   :return SymbolTree
            #  """
            #        f = StringIO.StringIO(mathml)
            #       return cls.parse_all_from_xml(f)[0]

    @classmethod
    def parse_from_mathml(cls, elem):
        """
        Parse expression from mathml


        :param elem mathml string

        :rtype SymbolTree
        :return SymbolTree

        """
        if (len(elem) == 0):
            return None

        elem_content = io.StringIO(elem)
        root = xml.etree.ElementTree.parse(elem_content).getroot()
        root = MathSymbol.parse_from_mathml(root)
        if root is not None:
            tree = cls(root)
            tree.mathml = elem
            return tree
        else:
            return None

    @classmethod
    def parse_all_from_xml(cls, filename, file_id, missing_tags=None, problem_files=None):
        """
        Parse expression from xml file


        :param filename Directory to index in
        :type  filename: string

        :rtype list(SymbolTree)
        :return list of Symbol trees found in file

        """

        trees = []

        def _extract_tree(tree, latex="", position=[]):
            try:

                if tree ==None:#if tree is latex convert to mathml
                    tree=LatexToMathML.convert_to_mathml(latex)


                symbol_tree = cls.parse_from_mathml(tree)
                if symbol_tree  is not None:
                    symbol_tree.latex = latex.strip()
                    symbol_tree.document = file_id
                    symbol_tree.position = position

                    trees.append(symbol_tree)
                else:
                    problem_files["unparsable_tree"]=  problem_files.get("unparsable_tree", set())
                    problem_files["unparsable_tree"].add(filename)
            except UnknownTagException as e:

                missing_tags[e.tag] = missing_tags.get(e.tag, set())
                missing_tags[e.tag].add(filename)

            except Exception as err:
                reason = str(err)
                problem_files[reason] = problem_files.get(reason, set())
                problem_files[reason].add(filename)



        moutput= MathExtractor.parse_from_xml(filename)


        latex_mathml=moutput.latex_mathml_dict
        math_token_positions = moutput.math_token_positions
        if 'no_latex' in latex_mathml:  # if tere are mathml with no corresponding latex
            for tree in latex_mathml.get('no_latex'):
                _extract_tree(tree)

        for latex, position in list(math_token_positions.items()):
            if latex is not None:
                latex = latex.strip()
            tree = latex_mathml.get(latex)
            _extract_tree(tree, latex, position)


        return trees


    @classmethod
    def count_tags(cls, directory):
        tags = Counter()
        fullnames = []
        if os.path.isfile(directory):
            fullnames.append(directory)
        else:
            for dirname, dirnames, filenames in os.walk(directory):
                fullnames.extend([os.path.join(dirname, filename)
                                  for filename
                                  in filenames
                                  if os.path.splitext(filename)[1] in ['.xhtml', '.xml', '.mathml']])

        for i, fullname in enumerate(fullnames):
            print(('parsing %s (%d of %d)' % (fullname, i + 1, len(fullnames))))
            for event, elem in ET.iterparse(fullname):
                if event == 'end' and elem.tag.startswith('{http://www.w3.org/1998/Math/MathML}'):
                    tags.update([elem.tag[36:]])
        for tag, count in tags.most_common():
            print(('%s, %d' % (tag, count)))


    @classmethod
    def parse_directory(cls, doc_mapping_list, combined_stats=Stats()):

        """
            Parse the symbols in the files in the directory

            :param directory Directory to index in
            :type  directory: string
            """

        combined_stats.num_documents += len(doc_mapping_list)

        def get():

            for (filename, doc_id,title) in doc_mapping_list:
                #print(('parsing %s, id:%d ' % (filename, doc_id)))
                try:
                    for t in cls.parse(filename, doc_id, missing_tags=combined_stats.missing_tags,
                                       problem_files=combined_stats.problem_files):
                        combined_stats.num_expressions += 1
                        yield t
                except Exception as err:
                    print("Failed to process document " + filename, file=sys.stderr)
                    print(str(err), file=sys.stderr)

        return get()


    @classmethod
    def parse_directory_old(cls, directory):
        """
            Parse the symbols in the files in the directory

            :param directory Directory to index in
            :type  directory: string
            """
        missing_tags = Counter()
        problem_files = {}
        fullnames = []
        if os.path.isfile(directory):
            fullnames.append(directory)
        else:
            for dirname, dirnames, filenames in os.walk(directory):
                fullnames.extend([os.path.join(dirname, filename) for filename in filenames])

            fullnames.sort()
        stats = {
            'num_documents': len(fullnames),
            'num_expressions': 0,
            'missing_tags': missing_tags,
            'problem_files': problem_files
        }

        def get():
            for i, fullname in enumerate(fullnames):
                print(('parsing %s (%d of %d)' % (fullname, i + 1, len(fullnames))))
                for t in cls.parse(fullname, missing_tags=missing_tags, problem_files=problem_files):
                    stats['num_expressions'] += 1
                    yield t

        return get(), stats


    def build_repr(self):
        builder = []
        self.root.build_repr(builder)
        builder = [b for b in builder if b]
        return 'SymbolTree(%s)' % ''.join(builder)


if __name__ == '__main__':
    trees = SymbolTree.parse_all(argv[1])
    print(('%d expressions parsed' % len(trees)))
