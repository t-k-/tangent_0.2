
"""
    Tangent
    Copyright (c) 2013 David Stalnaker, Richard Zanibbi

    This file is part of Tangent.

    Tanget is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tangent is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tangent.  If not, see <http://www.gnu.org/licenses/>.

    Contact:
        - David Stalnaker: david.stalnaker@gmail.com
        - Richard Zanibbi: rlaz@cs.rit.edu
"""
import concurrent
from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor
import csv
import json
import multiprocessing
import os
import sys
from tangent.common.search.model.Stats import Stats
from tangent.math.index.mysql_index_ntcir import MySQLIndexNtcir
from tangent.math.index.post_index_ntcir import PostIndexNTCIR
from tangent.math.models.symboltree import SymbolTree
from tangent.math.index.redisindex import RedisIndex
import time
from tangent.math.math_indexer_task import MathIndexer
from tangent.math.rankers.fmeasureranker import FMeasureRanker
from tangent.math.index.mysql_index import MySQLIndex
#import mysql.connector
#import oursql

from multiprocessing import Lock


import sys
sys.setrecursionlimit(10000)

import shutil

"""
Indexer is a standalone script that indexes a collection a d saves the index in a redis store

Supports mathml,.xhtml and tex files
"""

from sys import argv, exit


def print_help_and_exit():
    """
    Prints usage statement
    """

    exit('Usage: python index.py {index|second_pass|flush} doc_id_mapping databasename')


def math_indexer_task(pargs):
    database_collection, mappings,chunkid,lock = pargs
    name_id = chunkid
    math_index = PostIndexNTCIR(ranker=FMeasureRanker(), db=database_collection,process_id=name_id)

    print("Process %s has %s elements to process" % (name_id, len(mappings)))

    print ("Process %s  first element: %s"% (name_id,mappings[0]))
    print ("Process %s  last element: %s"% (name_id,mappings[-1]))
    combined_stats = Stats()

    trees = SymbolTree.parse_directory(mappings, combined_stats)
    math_index.add_all(trees)

    with lock:
        math_index.commit_all()

    return name_id


def chunks(l, n):
    """ Yield successive n-sized chunks from l.
    """
    counter=0
    for i in range(0, len(l), n):
        yield counter,l[i:i + n]
        counter=counter+1



def save_to_index(process_id,db):
    conn = oursql.connect(user='math_searcher', passwd='KEfks5sxIYAA7z7QnNkg', db=db,
                           use_unicode=True,
                           local_infile=True)

    curs =conn.cursor()
    filename=os.path.join(os.path.join(os.getcwd(), 'data'), "pairs."+str(process_id))

    sqlPairs ="""LOAD DATA local infile '"""  +filename + """' into table pairs CHARACTER SET utf8mb4 FIELDS TERMINATED BY '\\t' enclosed by '\\'' ESCAPED BY ''  LINES TERMINATED BY  '\\n'  """;
    filename=os.path.join(os.path.join(os.getcwd(), 'data'), "expression."+str(process_id))
    sqlExpression = """LOAD DATA local infile '"""  +filename + """' into table expression CHARACTER SET utf8mb4 FIELDS TERMINATED BY '\\t' enclosed by '\\'' ESCAPED BY ''  LINES TERMINATED BY  '\\n'  """;
    filename=os.path.join(os.path.join(os.getcwd(), 'data'),"expression_docs."+str(process_id))

    sqlDocs ="""LOAD DATA local infile '"""  +filename + """' into table expression_docs CHARACTER SET utf8mb4 FIELDS TERMINATED BY '\\t' enclosed by '\\'' ESCAPED BY ''  LINES TERMINATED BY  '\\n' """;


    #mysql load
    #mysqlimport --user="math_searcher" --password="KEfks5sxIYAA7z7QnNkg" --local --default-character-set="utf8mb4" --fields-terminated-by='\t' --fields-enclosed-by="\'" --fields-escaped-by="\'" math_ntcirtest *


    curs.execute("SET NAMES 'utf8';  ")
    curs.execute("SET CHARACTER SET utf8mb4; ")
    curs.execute(sqlPairs)
    #conn.commit()
    #curs.execute("SET NAMES 'utf8';  ")
    #curs.execute("SET CHARACTER SET utf8mb4; ")
    curs.execute(sqlExpression)
    #conn.commit()
    #curs.execute("SET NAMES 'utf8';  ")
    #curs.execute("SET CHARACTER SET utf8mb4; ")
    curs.execute(sqlDocs)
    conn.commit()


if __name__ == '__main__':



    if len(argv) > 2:

        start =time.time()
        step = argv[1]
        if step == 'index':  # index using file as reference



            doc_id_mapping_path = argv[2]
            database_name = argv[3]



            mappings = []
            with open(doc_id_mapping_path,  newline='',encoding='utf-8') as mapping_file:

                reader = csv.reader(mapping_file,  delimiter='\t',quotechar='\'', lineterminator='\n', quoting=csv.QUOTE_ALL)

                for row in reader:
                    mappings.append(row)


            num_docs=len(mappings)
            print("There are " + str(num_docs) + " documents to index")

            r = PostIndexNTCIR(ranker=FMeasureRanker(), db=database_name)
            r.flush()

            max_jobs =3
            chunk_size = 10000

            num_docs_remaining=num_docs
            manager = multiprocessing.Manager()
            lock = manager.Lock()

            args = [(database_name, small_chunk,chunkid,lock) for chunkid,small_chunk in chunks(mappings, chunk_size)]

             #delete and make directory again
            directory=os.path.join(os.getcwd(), 'data')
            if os.path.exists(directory):
                shutil.rmtree(directory)
            os.makedirs(directory)

            #for p in args:
            #    math_indexer_task(p)
            #    num_docs_remaining=num_docs_remaining-chunk_size
            #    print("Remaing docs %s" % (num_docs_remaining))



           



            with ProcessPoolExecutor(max_workers=max_jobs) as executor:
                for pid in executor.map(math_indexer_task, args):
                    print("%s is done parsing and saving to file" % (pid))
                    #save_to_index(pid,database_name)
                    print("%s is done saving to database" % (pid))
                    num_docs_remaining=max(num_docs_remaining-chunk_size,0)
                    print("Remaing docs %s of %s" % (num_docs_remaining,num_docs))



            print("Done indexing collection %s to %s" % (doc_id_mapping_path, database_name))

            end=time.time()
            elapsed=end-start

            print("Elapsed time %s"%(elapsed))

    else:
        print_help_and_exit()
