"""
    Tangent
    Copyright (c) 2013 David Stalnaker, Richard Zanibbi

    This file is part of Tangent.

    Tanget is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tangent is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tangent.  If not, see <http://www.gnu.org/licenses/>.

    Contact:
        - David Stalnaker: david.stalnaker@gmail.com
        - Richard Zanibbi: rlaz@cs.rit.edu
"""
from flask.ext.cors import cross_origin
import jsonpickle
from tangent.server import mathsearch_helper
from tangent.formatter.input_formatter import RawInputFormatter
from tangent.formatter.output_formtter import JsonOutputFormatter, RawOutputFormatter, NTCIR_Raw_OutputFormatter

import tangent.server.mathsearch_helper
from tangent.math.index.redisindex import RedisIndex
from tangent.math.models.symboltree import SymbolTree

"""
Tangent web app is a Flask based App

Below are the routes and appropriates actions

/query=?: query page if query else home page
/random : retrieve a random expression and query

"""

import time
from sys import argv, exit
import os
from operator import itemgetter
import urllib

from flask import Flask, render_template, request, make_response, send_file


app = Flask(__name__)
if len(argv) > 1:
    app.config.from_object(argv[1])
elif 'TANGENT_CONFIG' in os.environ:
    app.config.from_object(os.environ['TANGENT_CONFIG'])
else:
    print('Couldn\'t load config file')
    exit(0)

index = RedisIndex(db=app.config['DATABASE'], ranker=app.config['RANKER'], host="localhost")


def time_it(fn, *args):
    """
    Timing function
    """
    start = time.time()
    ret = fn(*args)
    end = time.time()
    return ((end - start) * 1000, ret)


# @app.route('/')
def root():
    """
    Base route

    if params['query' is passed, query it
    else return to home page
    """
    if 'query' in request.args:
        query_expr = request.args['query'].strip()
        # return combined_search(query_expr)
    else:
        return home()


@app.route('/tangentapi/combined/search')
@cross_origin(headers=['Content-Type'])
def combined_search_api(format='json',optional_args=None):
    """
    Base route

    if params['query' is passed, query it
    else return to home page
    """
    system = request.args.get('system')
    query = request.args.get('query')
    weight=request.args.get('weight')

    if query is None or system is None:
        return ""
    query_expr = RawInputFormatter.process(query)
    print("Passed query: %s"%(query_expr))

    ms = mathsearch_helper.MathSearcher(system=system, query=query_expr,weight=weight)
    response = ms.combined_search()

    formatter=None
    if format=='raw':
        formatter=RawOutputFormatter
    elif format=='json':
        formatter=JsonOutputFormatter
    elif format=='ntcir':
        formatter=NTCIR_Raw_OutputFormatter
    formatted_response=formatter.process(response,optional_args)
    return formatted_response

    # path="E:\\Copy\masters\\tangent\\tests\\samples\\sample_response.json"
    # with codecs.open(path, 'r', "utf-8") as myfile:
    #    content = myfile.read().replace('\n', ' ')

    #return content

    #return combined_search(query_expr,system)


@app.route('/tangentapi/mathsearch/')
@cross_origin(headers=['Content-Type'])
def math_search():
    """
    Base route

    if params['query' is passed, query it
    else return to home page
    """
    system = request.args.get('system')
    query = request.args.get('query')
    query_expr = query.strip()

    ms = mathsearch_helper.MathSearcher(system=system,query=query_expr)
    response = ms.math_search_expressions(query_expr)

    response_json = jsonpickle.encode(response)
    return response_json

    # path="E:\\Copy\masters\\tangent\\tests\\samples\\sample_response.json"
    # with codecs.open(path, 'r', "utf-8") as myfile:
    #    content = myfile.read().replace('\n', ' ')

    #return content

    #return combined_search(query_expr,system)




def home():
    """
    Home page
    """
    return render_template('new_query.html')


def query(query_expr):
    """
    Query the index given a query expression

    :type query_expr: string
    :param query_expr: query expression in tex and mathml
    """
    debug = 'debug' in request.args and request.args['debug'] == 'true'
    is_mathml = '<math' in query_expr
    if is_mathml:
        parse_time, tree = time_it(lambda f: SymbolTree.parse_from_mathml_string(f), query_expr)
    else:
        parse_time, tree = time_it(SymbolTree.parse_from_tex, query_expr)
    search_time, (results, num_results, pair_counts) = time_it(lambda: list(index.search(tree)))
    pair_count_str = u''

    # sort results by expression that share the most pair counts
    for p, c in sorted(pair_counts.items(), reverse=True, key=itemgetter(1)):
        pair_count_str += u'%s: %d, ' % (p, c)
    return render_template('results.html', query=query_expr, results=results, num_results=num_results,
                           pair_counts=pair_count_str, parse_time=parse_time, search_time=search_time, debug=debug)


@app.route('/tangentapi/compare/<latex_1>/<latex2>')
def get_tasks(latex_1, latex_2):
    expr = latex_1
    exp2 = latex_2


@app.route("/custom")
def test_main():
    return send_file('templates/my_results.html')


def query_mathml(query_expr):
    """
    Query the index for the given mathml expression

    :type query_expr: string
    :param query_expr: query expression in  mathml

    """
    parse_time, tree = time_it(lambda f: SymbolTree.parse_all_from_xml(f)[0], query_expr)
    search_time, (results, num_results, pair_counts) = time_it(lambda: list(index.search(tree)))
    pair_count_str = u''
    for p, c in sorted(pair_counts.items(), reverse=True, key=itemgetter(1)):
        pair_count_str += u'%s: %d, ' % (p, c)
    return render_template('results.html', query='', results=results, num_results=num_results,
                           pair_counts=pair_count_str, parse_time=parse_time, search_time=search_time)


@app.route("/simple")
def simple_display():
    my_obj = ""
    with open("dummy.json", "r") as myfile:
        data = myfile.read().replace('\n', '')
        my_obj = jsonpickle.decode(data)

    k = 7
    num_results = len(my_obj)
    search_time = 0
    parse_time = 0
    pair_count_str = ""

    results = my_obj
    return render_template('results_new.html', query='', results=results, num_results=num_results,
                           pair_counts=pair_count_str, parse_time=parse_time, search_time=search_time)


def new_display(hit_obj, query_expr, num_math_results, num_text_results, num_shared_results):
    search_time = 0
    parse_time = 0
    pair_count_str = ""
    query = query_expr

    results = jsonpickle.decode(hit_obj)
    num_results = len(results)
    return render_template('results_new.html', query=query, results=results, num_results=num_results,
                           pair_counts=pair_count_str, parse_time=parse_time, search_time=search_time,
                           num_math_results=num_math_results, num_text_results=num_text_results,
                           num_shared_results=num_shared_results)


@app.route('/random')
def random():
    """
    Search the index for a random expression
    """
    latex = index.random()
    return query(latex)




@app.template_filter('urlencode')
def urlencode(uri, **query):
    return urllib.quote(uri)


app.jinja_env.globals['urlencode'] = urlencode


def initialize(directory):
    """
    parse all the expression found in the index

    :type directory:string
    :param directory: directory where to index expressions from
    """
    trees, stats = SymbolTree.parse_directory(directory)
    index.add_all(trees)


def print_help_and_exit():
    """
    Usage
    """
    exit('Usage: python mathsearch.py config_object')


if __name__ == '__main__':
    if len(argv) > 1:
        if argv[1] == 'help':
            print_help_and_exit()

    # run app on host and port
    port = 9997
    host = app.config['HOST']
    app.run(port=port, host=host, processes=1)


