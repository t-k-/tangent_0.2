from itertools import chain
import re
import time
import platform

from tangent.common.hits_generation.HitResponse import HitResponse
from tangent.common.hits_generation.fragment_generator import BaseFragmentGenerator, FullFileFragmentGenerator
from tangent.common.hits_generation.hit_generator import HitGenerator
from tangent.common.hits_generation.modified_hit_generator import SentenceGraphFragmentGenerator
from tangent.common.search.metadataStore import Metadastore
from tangent.common.search.model.Hit import Hit
from tangent.common.search.model.MathResult import MathDocumentResult
from tangent.common.search.model.TextResult import TextDocumentHeader
from tangent.math.index.baseindex import Result
from tangent.math.index.mysql_index_ntcir import MySQLIndexNtcir
from tangent.math.models.exceptions import UnknownTagException
from tangent.other.position_mysql_index import PostionMySQLIndex
from tangent.math.models.symboltree import SymbolTree
from tangent.math.rankers.fmeasureranker import FMeasureRanker
from tangent.text import text_engine_client as tec


__author__ = 'Nidhin'

from nltk.stem import snowball


def time_it(fn, *args):
    """
    Timing function
    """
    start = time.time()
    ret = fn(*args)
    end = time.time()
    return ((end - start) * 1000, ret)


class Weights:
    @classmethod
    def default(cls):
        return [0.6,0.4,0.0]
    @classmethod
    def text_focused(cls):
        return [0.3,0.7,0]


    @classmethod
    def math_text_equal(cls):
        return [0.5,0.5,0]

    @classmethod
    def ntcir_default(cls):
        return [0.7,0.3,0]

    @classmethod
    def math_focused(cls):
        return [0.95,0.05,0]

    @classmethod
    def math_only(cls):
        return [1,0,0]



    @classmethod
    def weight_factory(cls,weight_str):


        if weight_str=='text_focused':
            return cls.text_focused()
        if weight_str=='math_text_equal':
            return cls.math_text_equal()
        if weight_str=='ntcir_default':
            return cls.ntcir_default()
        if weight_str =='math_focused':
            return cls.math_focused()
        if weight_str=='math_only':
            return cls.math_only()

        if weight_str=='default':
            return cls.default()

        raise Exception("Option %s is not recognized" %(weight_str))



class MathSearcher:
    __slots__ = ['system', 'query', 'full_query', 'text_query', 'math_query', 'cleaned_tokens', 'text_db', 'math_db',
                 'math_index', 'metadata_path','weight']

    def __init__(self, system, query,weight='default'):
        self.system = system
        self.full_query = query.strip()
        self.text_query, self.math_query, self.cleaned_tokens = self.__separate_query(self.full_query)

        self.weight=Weights.weight_factory(weight)

        running_system = platform.system()
        if system == 'Wikipedia':
            self.text_db = 'wikipedia'
            self.math_db = 'math_wikipedia'
            self.metadata_path = "E:\\ntcir\\ntcir_wikipedia_filemapping_windows.txt"
            if 'Linux' in running_system:
                self.metadata_path = '/mnt/raid/ntp5633/datasets/file_mapping/ntcir11_wikipedia.txt'
        elif system == 'NTCIR Actual':
            self.text_db = 'ntcir'
            self.math_db = 'math_ntcir'
            self.metadata_path = "E:\\ntcir\\ntcir11_filemapping.txt"
            if 'Linux' in running_system:
                self.metadata_path = '/mnt/raid/ntp5633/datasets/file_mapping/ntcir11_filemapping.txt'
        elif system == 'NTCIR Test':
            self.text_db = 'ntcir_test'
            self.math_db = 'math_ntcirtest'
            self.metadata_path = "D:\\masters\\file_mapping\\ntcir_subset.json"
            if 'Linux' in running_system:
                self.metadata_path = '/mnt/raid/ntp5633/datasets/file_mapping/ntcir_subset.json'
        elif system == 'MREC':
            self.text_db = 'mrec'
            self.math_db = 'math_mrec'
            self.metadata_path = "E:\\masters\\file_mapping\\mrec.json"
            if 'Linux' in running_system:
                self.metadata_path = '/mnt/raid/ntp5633/datasets/file_mapping/mrec.json'
        else:
            raise Exception("Unknonwn collection %s" % (system))


        # self.math_index = PostIndexNTCIR(ranker=FMeasureRanker(), db=self.math_db)


        if system=='NTCIR Actual':
            self.math_index = None
        else:
            self.math_index = MySQLIndexNtcir(ranker=FMeasureRanker(), db=self.math_db)


    def __separate_query(self, query):
        """
        Splits a query string into math and text subparts

        :type query: string
        :param query: query expression containing text and math

        :returns: a tuple of text and list of math index terms
        :rtype: (str,list(str))
        """
        split_pattern = u"<math.*?>.*?</math>|\\$+.*?\\$+|[^<\\s]+"
        split_pattern = re.compile(split_pattern, re.DOTALL)
        tokens = split_pattern.findall(query)

        math_query = {}
        text_query = {}
        text_query_str = ""
        cleaned_tokens = []
        stemmer = snowball.EnglishStemmer()
        i = 0
        for t in tokens:
            if t.startswith("$"):
                t = t.replace("$", "")
                math_query[t] = i
                cleaned_tokens.append('<math>' + t + '</math>')
            else:
                t = stemmer.stem(word=t)
                text_query[t] = i
                text_query_str = text_query_str + " " + t
                cleaned_tokens.append(t)
            i += 1

        return text_query_str, math_query, cleaned_tokens

    def combined_search(self, max_results=15, fragment_generator=FullFileFragmentGenerator) -> HitResponse:

        """

        :type max_results: int
        :param max_results:
        :type fragment_generator: BaseFragmentGenerator
        :param fragment_generator:
        :return: HitResponse
        """

        all_docs = set()
        math_results = None
        text_results = None
        num_math_results = 0
        num_text_results = 0
        shared_results = 0
        total_search_pair_count = 0

        if self.math_query:

            query=list(self.math_query.keys())[0]
            parse_time, search_time, math_results, total_search_pair_count = self.math_search_expressions(query)
            #parse_time, search_time, math_results, total_search_pair_count = self.math_search_documents()
            print("Got %s math results" %(len(math_results.keys())))
            all_docs.update(math_results.keys())
            num_math_results = len(math_results)
        if self.text_query:
            # full_query = " ".join(self.cleaned_tokens)
            full_query = self.text_query
            text_results = tec.get_results(full_query, system=self.text_db)
            assert isinstance(text_results, TextDocumentHeader)
            print("Got %s text results" %(len(text_results.doc_id_paths.keys())))
            all_docs.update(text_results.doc_id_paths.keys())
            num_text_results = text_results.numResults
        print("Got text and math results")
        shared_results = len(all_docs)
        hit_list = []
        for doc in all_docs:
            h = HitGenerator.create_hit_result(cleaned_tokens=self.cleaned_tokens, doc=doc, math_query=self.math_query,
                                               math_results=math_results, text_query=self.text_query,
                                               text_results=text_results,
                                               total_search_pair_count=total_search_pair_count)
            assert isinstance(h, Hit)

            mweight,tweight,pweight=self.weight
            h.final_score = mweight * float(h.math_score) + tweight * float(h.text_score) + pweight * float(h.pos_score)
            hit_list.append(h)

        hit_list = sorted(hit_list, key=lambda elem: elem.final_score,
                          reverse=True)


        # first 20 results
        hit_list = hit_list[0:max_results]

        mds = Metadastore(self.metadata_path)

        print("Generating fragments")

        for hit in hit_list:
            assert isinstance(hit, Hit)
            hit.full_path = mds.get_path_for_id(hit.id)
            hit.title = mds.get_file_name_for_id(hit.id)

            hit_specific_list = []

            for term in hit.token_positions.keys():
                hit_specific_list.append(term)



            hit.fragment = fragment_generator.generate_snippet_from_file(path=hit.full_path,
                                                                         hit_specific_list=hit_specific_list)

            if hit.fragment:
                hit.fragment = hit.fragment.replace("<math>", "\\(")
                hit.fragment = hit.fragment.replace("</math>", "\\)")

        hit_response = HitResponse(hit_list=hit_list, num_text_results=num_text_results,
                                   num_math_results=num_math_results,
                                   shared_results=shared_results, query_expression=self.full_query,
                                   parsed_math_query=self.math_query, parsed_text_query=self.text_query)

        return hit_response


    def math_search_documents_single(self):
        query_expr = list(self.math_query.keys())
        doc_mapping = {}
        total_parse_time = 0
        total_search_time = 0
        total_search_pair_count = 0
        for idx, q in enumerate(query_expr):
            try:
                parse_time, search_time, results, num_possible_expressions, search_pairs = self.math_search_expressions(q)
                total_parse_time = total_parse_time + parse_time
                total_search_time = total_search_time + search_time
                total_search_pair_count = total_search_pair_count + len(search_pairs)

                for r in results:
                    # assert isinstance(r, Result)
                    positions = r.doc_positions
                    links = r.links

                    for d in links:
                        d_str = str(d)
                        if d_str not in doc_mapping:
                            doc_mapping[d_str] = MathDocumentResult(d_str, num_expressions=len(query_expr))
                        doc_mapping[d_str].add_expression(idx, r.latex, r.score, r.expr_id, "", search_pairs)
                        doc_mapping[d_str].positions[r.latex] = positions[d]
            except UnknownTagException as e:
                print("Failed to parse query %s" %(q))
                print (str(e))

        return total_parse_time, total_search_time, doc_mapping, total_search_pair_count

    def math_search_documents(self):
        if self.math_db == 'math_ntcir':
             return self.math_search_documents_nticr()
            #return self.math_search_documents_single()
        else:


            return self.math_search_documents_single()


    def math_search_documents_nticr(self):
        base = 'math_ntcir_a'
        databases = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i']
        #databases = ['i']

        total_parse_time = 0
        total_search_time = 0
        total_search_pair_count = 0
        global_dictionary_results = {}
        for d in databases:

            full_database_name = base + d

            mseaarcher = MySQLIndexNtcir(full_database_name)
            self.math_index = mseaarcher
            parse_time, search_time, doc_mapping, total_search_pair_count = self.math_search_documents_single()

            print ("Found %s results in database %s"%(len(doc_mapping),d))
            global_dictionary_results = dict(chain(global_dictionary_results.items(), doc_mapping.items()))
            total_search_pair_count = total_search_pair_count
            total_search_time = total_search_time + search_time
            total_parse_time = total_parse_time + parse_time

        return total_parse_time, total_search_time, global_dictionary_results, total_search_pair_count


    def math_search_expressions(self, query_expr_):
        query_expr = query_expr_
        parse_time, tree = time_it(SymbolTree.parse_from_tex, query_expr)
        print(query_expr_)
        con = SymbolTree.parse_from_tex(query_expr)
        if con is not None:
            print("Parsed symbol tree")
        else:
            print("Failed to create symbol tree")

        search_time, (results, num_possible_expressions, search_pairs, _) = time_it(
            lambda: list(self.math_index.search(tree)))

        return parse_time, search_time, results, num_possible_expressions, search_pairs




    def math_search_expressions_wikipedia(self, query_expr):
        """

        :param query_expr:
        :return: list[Result]
        """
        parse_time, tree = time_it(SymbolTree.parse_from_tex, query_expr)
        print(query_expr)
        con = SymbolTree.parse_from_tex(query_expr)
        if con is not None:
            print("Parsed symbol tree")
        else:
            print("Failed to create symbol tree")

        search_time, (results, num_possible_expressions, search_pairs, _) = time_it(
            lambda: list(self.math_index.search(tree)))

        return results

