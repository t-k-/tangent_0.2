from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
import csv
from genericpath import isfile
from os import listdir
import os
from os.path import join

__author__ = 'Nidhin'

from bs4 import BeautifulSoup

query_file = "E:\\data\\masters\\datasets\\ntcir-11-wmc-1b\\v3\\00004.html"

expected_latex = "{\displaystyle A}"


"""
The current system does not support extracting expression ids in the mathml document

This script given the results latex, finds the expression id that has the same altex
"""

def get_doc_id_mapping(filemapping:str) -> dict:
    mappings = {}
    with open(filemapping, newline='', encoding='utf-8') as mapping_file:
        reader = csv.reader(mapping_file, delimiter='\t', quotechar='\'', lineterminator='\n', quoting=csv.QUOTE_ALL)

        for row in reader:
            docpath, docid, doctitle = row
            mappings[docid] = docpath

    return mappings


def find_expression_id_in_doc(expression_latex:str, query_file_path:str) -> str:
    """
    Searches the whole docuemtn for expression that matcehs query
    :param expression_latex:
    :param query_file_path:
    :return:
    """
    with open(query_file_path, encoding='utf-8') as file:
        parsed = BeautifulSoup(file)

    all_math_in_doc = parsed.findAll("math")
    """:type : list(bs4.element.Tag)"""

    for me in all_math_in_doc:
        """:type : bs4.element.Tag"""

        alttext = me.get("alttext")

        if alttext == expression_latex:
            return me.get("id")


def query_document(args):

    doc_path, doc_dict=args
    head,tail=os.path.split(doc_path)

    doc_path_id_path = "E:\\tanget-update\\results_cleaned\\"+tail[0:-3]+"csv"

    expressions_added=0
    with open(doc_path_id_path, 'w') as w:
        with open(doc_path, 'r') as f:
            reader = csv.reader(f, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)
           # w.write("%s,%s\n" % ("queryId","formulaId"))
            for row in reader:
                if expressions_added<=200:
                    query_num, expr_id, doc_id, score, latex = row
                    doc_path = doc_dict[doc_id]
                    new_exp_id = find_expression_id_in_doc(latex, doc_path)
                    query_id=query_num.split("-")[-1]
                    if new_exp_id is not None:
                        new_exp_id=new_exp_id.split(".")
                        #new_exp_id[-1]="0"
                        new_exp_id=".".join(new_exp_id)
                        w.write("%s,%s\n" % (query_id,new_exp_id))
                        #print(new_exp_id)
                        expressions_added=expressions_added+1

    return doc_path_id_path

if __name__ == '__main__':

    wikipedia_file_mapping_doc_id = "E:\\ntcir\\ntcir_wikipedia_filemapping_windows.txt"
    results_file = "E:\\tanget-update\\results\\math_only_NTCIR11-Math-100.tsv"

    doc_dict = get_doc_id_mapping(wikipedia_file_mapping_doc_id)

    query_document((results_file,doc_dict))


    mypath = "E:\\tanget-update\\results"
    onlyfiles = [os.path.join(mypath,f) for f in listdir(mypath) if isfile(join(mypath, f))]

    args = [(filepath, doc_dict) for filepath in onlyfiles]

    #with ProcessPoolExecutor() as executor:
    with ProcessPoolExecutor() as executor:
        for file in executor.map(query_document, args):
            print("Processed %s " % (file))



