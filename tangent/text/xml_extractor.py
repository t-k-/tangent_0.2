import codecs
import string
import xml
from tangent.common.utility.math_extractor import MathExtractor

from tangent.text.models import Document
from tangent.text.title_store import TitleStore
import xml.etree.ElementTree as etree

import os


__author__ = 'Nidhin'


class GenericXMLExtractor(object):
    def __init__(self):
        pass


    def parse_document(self, file_path) -> Document:
        """
        :rtype Document
        """
        with codecs.open(file_path, 'r', "utf-8") as myfile:
            content = myfile.read().replace('\n', ' ')

        d = Document()
        d.math_content=self.parse_math_content(content)

        if d.math_content is None:
            d.math_content=""

        return d


    def parse_math_content(self, content)->string:
        """

        :rtype : string
        """

        moutput=MathExtractor.input_cleaner(content)


        return moutput.cleaned_file_content



class MRecXMLExtractor(GenericXMLExtractor):
    def parse_document(self, file_path):
        """
        :rtype Document
        """
        with codecs.open(file_path, 'r', "utf-8") as myfile:
            content = myfile.read().replace('\n', ' ')

        tree=etree.parse(file_path)
        assert isinstance(tree, xml.etree.ElementTree.ElementTree)

        d = Document()

        d.title = tree.getroot()[0][0].text

        doc_content=self.parse_math_content(content)

        d.math_content=doc_content
        return d


class WikipediaXMLExtractor(GenericXMLExtractor):
    def __init__(self, title_store):
        assert isinstance(title_store, TitleStore)
        self.title_store = title_store

    def parse_document(self, file_path) -> Document:
        """
        :rtype Document
        """
        with codecs.open(file_path, 'r', "utf-8") as myfile:
            content = myfile.read().replace('\n', ' ')

        tree = xml.etree.ElementTree.parse(content)
        assert isinstance(tree, xml.etree.ElementTree.ElementTree)

        d = Document()
        #d.title = self.title_store.get_title()

        d.math_content=self.parse_math_content(content)

        return d