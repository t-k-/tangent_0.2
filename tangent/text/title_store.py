__author__ = 'Nidhin'
import csv


class TitleStore(object):



    def __init__(self,file_path):
        self.mapping={}

        with open(file_path, 'rb') as csvfile:
            file_reader = csv.reader(csvfile, delimiter=',', quotechar='|')

            for row in file_reader:
                self.mapping[row[0]]=row[1]



    def get_title(self,document_id):
        return self.mapping[document_id]

