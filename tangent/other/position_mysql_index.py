from collections import defaultdict, Counter
import simplejson
from operator import itemgetter
import sys
import zlib
from tangent.common.hits_generation.modified_hit_generator import SentenceGraph

from tangent.math.index.baseindex import BaseIndex, Result
from tangent.math.rankers.fmeasureranker import FMeasureRanker

MAX_UNCOMMITED = 5000

__author__ = 'Nidhin'

import pymysql as mdb


class PostionMySQLIndex:
    def __init__(self, collection, port=3306, host='127.0.0.1'):
        conn = mdb.connect(host=host, port=port, user='math_searcher', passwd='KEfks5sxIYAA7z7QnNkg',
                           db='math_doc_summaries',
                           use_unicode=True,
                           charset='utf8mb4')

        self.cursor = conn.cursor()
        self.conn = conn

        self.conn.autocommit(False)
        self.collection = collection

        if (self.collection=='Wikipedia'):
            return 'wikipeida'
        self._non_commited = 0
        self.documents_to_commit = []
        self.cursor.execute("SET NAMES 'utf8';  SET CHARACTER SET utf8mb4;")


    def flush(self):

        self.cursor.execute("DELeTE from  summaries where collection_id = %s",[self.collection])
        self.conn.commit()


    def add_document(self, doc_id, summary_graph):
        assert isinstance(summary_graph,str)

        compressed_str=zlib.compress(summary_graph.encode('utf-8'))

        self.documents_to_commit.append((doc_id, self.collection, compressed_str))

        if len(self.documents_to_commit)>10:
            self.commit()


    def get_summary(self, doc_id):
        self.cursor.execute(
            "Select summary_graph from summaries where doc_id=%s and collection_id=%s",[doc_id,self.collection])

        summary_row = self.cursor.fetchone()
        if summary_row is not None:
            summary_graph=summary_row[0]
            summary_graph_decompressed=zlib.decompress(summary_graph).decode('utf-8')
            return summary_graph_decompressed
        return None

    def commit(self):

        try:
            self.cursor.executemany("""insert into summaries(doc_id,collection_id,summary_graph) values(%s,%s,%s)
                                    """,
                                    self.documents_to_commit)
        except Exception as err:
            print("Failed to add " + str(len(self.documents_to_commit)) + " summaries", file=sys.stderr)
            print("Failed to add document such as:" + str(self.documents_to_commit[0]), file=sys.stderr)
            print(str(err), file=sys.stderr)


        # for p in self.pairs_to_commit:



        self.conn.commit()
        self.documents_to_commit = []
        self._non_commited = 0


    def commit_all(self):
        self.commit()
