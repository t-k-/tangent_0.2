__author__ = 'Nidhin'


class HitResponse(object):

    def __init__(self,hit_list=[],num_text_results=0,num_math_results=0,shared_results=0,query_expression="",parsed_math_query="",parsed_text_query=""):
        self.hit_list=hit_list
        self.num_text_results=num_text_results
        self.num_math_results=num_math_results
        self.shared_results=shared_results
        self.query_expression=query_expression
        self.parsed_math_query=parsed_math_query
        self.parsed_text_query=parsed_text_query