from tangent.common.hits_generation.position_scorer import NaivePoistionSocrer
from tangent.common.search.model.Hit import Hit
from tangent.common.search.model.MathResult import MathDocumentResult
from tangent.common.search.model.TextResult import TextDocumentHeader

__author__ = 'Nidhin'



class HitGenerator (object):



    @classmethod
    def create_hit_result(cls,cleaned_tokens, doc, math_query, math_results, text_query, text_results,total_search_pair_count):
        h = Hit()
        #mds = Metadastore()
        h.token_positions = {}

        h.id = doc

        if math_query and doc in math_results:
            """:type MathDocumentResult"""
            math_doc = math_results[doc]
            assert isinstance(math_doc, MathDocumentResult)

            h.math_score = math_doc.calculate_score(total_search_pair_count)
            h.math_reason = math_doc.get_math_reason()

            for e in math_doc.get_found_expressions():
                e_pos='<math>'+e+'</math>'
                h.token_positions[e_pos] = math_doc.positions[e]

        if not math_query:
            h.math_score=1

            #h.token_positions[h.math_reason]=text_results.doc_positions[h.math_reason]
        if text_query and doc in text_results.doc_id_paths.keys():
            assert isinstance(text_results, TextDocumentHeader)
            h.raw_text_score = text_results.doc_score[doc]
            h.text_score = h.raw_text_score/text_results.maxScore
            h.text_reason = ""
            for q in text_query.split():
                token_positions = text_results.doc_positions.get(doc,{})
                if q in token_positions:  #else it was possibly a stop word
                    h.token_positions[q] = token_positions[q]
                    h.text_reason = h.text_reason + " " + q + ":found"  #word found
                else:
                    h.text_reason = h.text_reason + " " + q + ":not_found"

        if not text_query:
            h.text_score=1
            h.raw_text_score=1
        #h.fragment = generate_snippet_from_file(mds.get_path_for_id(doc), h.token_positions)
        h.pos_score, h.pos_reason = NaivePoistionSocrer.calculate_position_score(cleaned_tokens, h.token_positions)
        return h