import codecs

from stemming.porter2 import stem
from tangent.common.hits_generation.fragment_generator import BaseFragmentGenerator

from tangent.common.utility.math_extractor import MathExtractor


__author__ = 'Nidhin'


class NaiveFragmentGenerator(BaseFragmentGenerator):


    @classmethod
    def generate_snippet_from_file(cls,path, term_positions={},hit_specific_list=None):
        with open(path, "r", encoding='utf-8') as myfile:
            file_content = myfile.read().replace("\n", "")

        moutput = MathExtractor.input_cleaner(file_content)
        content = moutput.cleaned_file_content

        return cls.generate_snippet(content, term_positions)



    @classmethod

    def generate_snippet(cls,file_content, term_positions={},hit_specific_list=None):
        """
        Generates the best 2 fragments that are composed of 10 tokens which has the most words in the query
        :param file_content:
        :param term_positions:
        :param hit_specific_list:
        :return:
        """
        interested_fragments = []
        split_pattern = u"<math.*?>.*?</math>|\\$+.*?\\$+|[^<\\s]+"
        import re

        split_pattern = re.compile(split_pattern, re.DOTALL)
        #split tokens on math and space
        tokens = split_pattern.findall(file_content)

        for t in term_positions.keys():
            #TODO fix solr position
            indices = [i for i, x in enumerate(tokens) if stem(x) == t]
            for pos in indices:
            #for pos in term_positions[t]:
                fragment = tokens[pos - 5:pos + 5]
                score = 0

                for word in fragment:
                    if stem(word) in term_positions:
                        score += 1
                interested_fragments.append((fragment, score, pos - 5, pos + 5))

        interested_fragments = sorted(interested_fragments, key=lambda elem: elem[1],
                                      reverse=True)

        max_boundary = -1

        found_fragments = 0

        replace_math_pattern = u"<math.*?>.*?</math>"
        replace_math_pattern = re.compile(replace_math_pattern, re.DOTALL)

        idx = 0
        fragment = ""


        def reg_cleaner_helper(input):
            """
            Replace math tag with latex content
            :param input:
            :return:
            """
            input = input.group(0)
            if input.startswith(u"<math>") and input.endswith(u"</math>"):
                input = input.replace(u"<math>", "")
                input = input.replace(u"</math>", "")
                input = input.replace(u"</math>", "")
                input = "$" + input + "$"
            return input


        #take the first two non continous fragments
        while (found_fragments < 2 and idx < len(interested_fragments)):
            content, score, start, end = interested_fragments[idx]
            if start > max_boundary:  #doesn't share indices with previous fragment
                content = " ".join(content)
                content = replace_math_pattern.sub(reg_cleaner_helper, content)
                fragment += "..." + content
                found_fragments = found_fragments + 1
            max_boundary = end
            idx = idx + 1

        return fragment
