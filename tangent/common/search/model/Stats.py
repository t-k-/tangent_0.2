from collections import namedtuple

__author__ = 'Nidhin'


class Stats(object):
    __slots__ = ["num_documents", "num_expressions", 'missing_tags', 'problem_files']

    def __init__(self):
        self.num_documents=0
        self.num_expressions=0
        self.missing_tags={}
        self.problem_files={}
