import os

import redis
import nose.tools as nt



__author__ = 'ntp5633'

from spec import Spec
from tangent.math.index.redisindex import RedisIndex
from tangent.math.rankers.fmeasureranker import FMeasureRanker

from tangent.math import math_indexer_post



import config


test_database = 5


class RedisDocument_Test(Spec):
    def setup(self):
        #flush the test databse
        r = redis.StrictRedis(db=test_database)
        r.flushdb()

        #create new test database
        self.redis_index = RedisIndex(db=test_database)

        #tests doc has tree douments

        path = os.path.abspath("small_test_data")
        math_indexer_post.index(path, self.redis_index)


        #   Index i = Index.
        self.config = config.Config()
        self.config.DATABASE = test_database
        self.config.RANKER = FMeasureRanker()


    def test_indexed_correct_symbols_pairs_and_expressions(self):
        r = redis.StrictRedis(db=test_database)

        identifed_expressions = 443
        #identified expressions
        doc_keys = r.keys(pattern="expr:*:doc")
        assert (len(doc_keys) == identifed_expressions)


        # next_expr_id should be 443
        next_id = r.get("next_expr_id")
        assert (next_id == str(identifed_expressions))



        #mathmml
        assert (len(r.keys(pattern="expr:*:mathml")) == identifed_expressions)
        #tree:symbol ->expression id
        assert (len(r.keys(pattern="tree:Symbol*")) == identifed_expressions)

        #num of expr_scores
        assert (len(r.keys(pattern="expr:*score")) == 3104)


        #correct paths
        assert (len(r.keys(pattern="pair:*paths")) == 13854)

        #total keys
        assert (len(r.keys(pattern="*")) == 33384)


        #correct exprs
        assert (len(r.keys(pattern="pair:*exprs")) == 13854)


    def test_expr_that_has_multiple_results(self):
        query_expr_latex = "k!"

        index = RedisIndex(db=self.config.DATABASE, ranker=self.config.RANKER)
        results, _, _ = index.search_tex(query_expr_latex)

        closest_match = results[0]
        #4 results for query
        assert (len(closest_match.links) == 4)

        expected_docs = set(['Wikipedia - ' + "Passcals triangle",
                             'Wikipedia - ' + "Proofs of Fermats theorem on sums of two squares",
                             'Wikipedia - ' + "Color-coding",
                             'Wikipedia - ' + "Analytic combinatorics"]);

        actual_docs = set([l[1] for l in closest_match.links])

        nt.eq_(expected_docs,actual_docs,"Documents don't match")



    def test_expr_that_has_two_results(self):
        query_expr_latex = "u_{\\lambda}!"

        index = RedisIndex(db=self.config.DATABASE, ranker=self.config.RANKER)
        results, _, _ = index.search_tex(query_expr_latex)

        closest_match = results[0]
        #4 results for query
        assert (len(closest_match.links) == 2)

        expected_docs = set(['Wikipedia - ' + "Hall algebra",
                             'Wikipedia - ' + "Sobolev conjugate"]);

        actual_docs = set([l[1] for l in closest_match.links])

        nt.eq_(expected_docs,actual_docs,"Documents don't match")


    def test_expr_that_has_three_results(self):
        query_expr_latex = "2^{5}"

        index = RedisIndex(db=self.config.DATABASE, ranker=self.config.RANKER)
        results, _, _ = index.search_tex(query_expr_latex)

        closest_match = results[0]
        #4 results for query
        assert (len(closest_match.links) == 3)

        expected_docs = set(['Wikipedia - ' + "Goodsteins theorem",
                             'Wikipedia - ' + "32 (number)",
                             'Wikipedia - ' + "Dempwolff group"
                             ]);

        actual_docs = set([l[1] for l in closest_match.links])

        nt.eq_(expected_docs,actual_docs,"Documents don't match")



    def test_expr_that_has_one_result(self):
        query_expr_latex = "\\displaystyle\\varphi(3^{k})=\\varphi(2\\times 3^{k})=2\\times 3^{{k-1}}."

        index = RedisIndex(db=self.config.DATABASE, ranker=self.config.RANKER)
        results, _, _ = index.search_tex(query_expr_latex)

        closest_match = results[0]
        #4 results for query
        assert (len(closest_match.links) == 1)

        expected_docs = set(['Wikipedia - ' + "Perfect totient number"]);

        actual_docs = set([l[1] for l in closest_match.links])

        nt.eq_(expected_docs,actual_docs,"Documents don't match")