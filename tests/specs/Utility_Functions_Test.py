
__author__ = 'ntp5633'
import nose.tools as nt
from spec import Spec

import tangent.common.utility as ut


class VBYTE_Test(Spec):

    def test_encoding(self):

        num1=824
        num2=5
        num3=214577

        num1_encoded=1720
        num2_encoded=133
        num3_encoded=855217


        actual_num1=ut.vbyte_encode(num1)
        actual_num2=ut.vbyte_encode(num2)
        actual_num3=ut.vbyte_encode(num3)


        nt.assert_equals(num1_encoded,actual_num1)
        nt.assert_equals(str(num1_encoded),str(actual_num1))
        nt.assert_equals(str(num2_encoded),str(actual_num2))
        nt.assert_equals(str(num3_encoded),str(actual_num3))




    def test_encoding_zero(self):

        num1=0

        num1_encoded=str(bytearray([128]))

        actual_num1=ut.vbyte_encode(num1)


        nt.assert_equals(num1_encoded,actual_num1)


    def test_decoding(self):

        num1_encoded=str(bytearray([6,184]))
        num2_encoded=str(bytearray([133]))
        num3_encoded=str(bytearray([13,12,177]))

        expected_num1=824
        expected_num2=5
        expected_num3=214577

        actual_num1=ut.vbyte_decode(num1_encoded)
        actual_num2=ut.vbyte_decode(num2_encoded)
        actual_num3=ut.vbyte_decode(num3_encoded)

        nt.assert_equals(expected_num1,actual_num1)
        nt.assert_equals(expected_num2,actual_num2)
        nt.assert_equals(expected_num3,actual_num3)


    def test_encoding_list(self):
        l = [824,829,215406]

        num1_encoded=str(bytearray([6,184]))
        num2_encoded=str(bytearray([133]))
        num3_encoded=str(bytearray([13,12,177]))

        expected_encoded_l=[num1_encoded,num2_encoded,num3_encoded]
        actual_encoded_l=ut.vbyte_encode_list(l)
        nt.assert_equals(expected_encoded_l,actual_encoded_l)


    def gap_encoded_test(self):
        l = [824,829,215406]
        expeced_l=[824,5,214577]
        actual=ut.gap_endoded_list(l)

        nt.assert_equals(actual,expeced_l)

    def run_length_compress(self):
        l='aaaabccaadeeee'
        expected='4ab2c2ad4e'

        actual=ut.run_length_encoded(l)

        nt.assert_equal(actual,expected)

