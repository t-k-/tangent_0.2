from spec import Spec
from tangent.math.models.symboltree import SymbolTree
from tangent.math.rankers.fmeasureranker import FMeasureRanker
import nose.tools as nt

__author__ = 'Nidhin'


class Wildcard_Test(Spec):
    def checkEqual(self, L1, L2):
        return len(L1) == len(L2) and sorted(L1) == sorted(L2)

    def test_simple_parses_wildcard(self):
        latex = "a^{?i}"
        sp = SymbolTree.parse_from_tex(latex, -1)
        query_pairs = sp.get_pairs()
        exp_pairs = ['a|qvar_i|1|1', 'qvar_i|None|0|0']

        assert (self.checkEqual(query_pairs, exp_pairs))

        #unifies to number
        unified_latex = "a^{999}"
        exp_pairs = SymbolTree.parse_from_tex(unified_latex, -1).get_pairs()
        score, _, _, _ = FMeasureRanker.rank2(exp_pairs, query_pairs)

        nt.eq_(1,score)


        #unifies to symbol
        unified_latex = "a^{z}"
        exp_pairs = SymbolTree.parse_from_tex(unified_latex, -1).get_pairs()

        score, _, _, _ = FMeasureRanker.rank2(exp_pairs, query_pairs)

        nt.eq_(1,score)


    def test_no_wildcard_is_still_working(self):
        latex = "a^x+b^x+c^x=1"
        other_latex = "a^x+b^x+c^x=1"

        original_pairs = SymbolTree.parse_from_tex(latex, -1).get_pairs()
        other_pairs = SymbolTree.parse_from_tex(other_latex, -1).get_pairs()

        score, _, _, _ = FMeasureRanker.rank2(original_pairs, other_pairs)

        nt.eq_(1,score)


    #should not match because not same base and power
    def test_pair_not_correct_structure(self):
        latex = "?a^?a"
        other_latex = "7^8"

        query_pairs = SymbolTree.parse_from_tex(latex, -1).get_pairs()
        exp_pairs = SymbolTree.parse_from_tex(other_latex, -1).get_pairs()

        score, _, _, _ = FMeasureRanker.rank2(exp_pairs, query_pairs)

        nt.eq_(score,0)

    def test_pair_correct_structure(self):
        latex = "?a^?a"
        other_latex = "7^7"

        query_pairs = SymbolTree.parse_from_tex(latex, -1).get_pairs()
        exp_pairs = SymbolTree.parse_from_tex(other_latex, -1).get_pairs()

        score, _, _, _ = FMeasureRanker.rank2(exp_pairs, query_pairs)

        nt.eq_(1,score)


    def test_pair_not_correct_structure2(self):
        latex = "a+?i+?i"
        other_latex = "a+7+6"

        query_pairs = SymbolTree.parse_from_tex(latex, -1).get_pairs()
        exp_pairs = SymbolTree.parse_from_tex(other_latex, -1).get_pairs()

        score, _, _, _ = FMeasureRanker.rank2(exp_pairs, query_pairs)

        nt.eq_(0,score)


    def test_pair_correct_structure2(self):
        latex = "a+?i+?i"
        other_latex = "a+6+6"

        query_pairs = SymbolTree.parse_from_tex(latex, -1).get_pairs()
        exp_pairs = SymbolTree.parse_from_tex(other_latex, -1).get_pairs()

        score, _, _, _ = FMeasureRanker.rank2(exp_pairs, query_pairs)

        nt.eq_(score,1)




    def test_partial_match_query_has_more(self):
        latex = "a^?i"
        other_latex = "a"
        query_pairs = SymbolTree.parse_from_tex(latex, -1).get_pairs()
        exp_pairs = SymbolTree.parse_from_tex(other_latex, -1).get_pairs()
        score, _, _, _ = FMeasureRanker.rank2(exp_pairs, query_pairs)

        nt.eq_(score,0)

    def test_partial_match_query_has_less(self):
        latex = "a^{?i}"
        #latex_pairs=['a|qvar_i|1|1', 'qvar_i|None|0|0']
        other_latex = "a^{7}+9"
        #other pairs=['a|7|1|1', '7|None|0|0', 'a|+|1|0', 'a|9|2|0', '+|9|1|0', '9|None|0|0']
        query_pairs = SymbolTree.parse_from_tex(latex, -1).get_pairs()
        exp_pairs = SymbolTree.parse_from_tex(other_latex, -1).get_pairs()
        score, _, _, _ = FMeasureRanker.rank2(exp_pairs, query_pairs)

        nt.eq_(0.5,score)
